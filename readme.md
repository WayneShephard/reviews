# Модуль "Отзывы"

## Предпосылки

В приложении МОБД ЭКБ КП для компаний необходимо подсчитывать средний рейтинг основываясь на оценка проставляемых пользователями. 

При этом, необходимо было учитывать следующее:

1. каждый пользователь может оставить только одну оценку;
2. пользователь может изменить свою оценку через какое то время;
3. оценка должна быть в пределах от 1 до 5 (желательно использование компонента проставления рейтинга).

Реализация данного функционала достаточно объемна (из-за GWT компонента), а функционал модуля может быть обобщен. В связи с этим было решено выделить данный функционал в отдельный модуль.



## Установка

### Установка локально

`**gradlew insall**`

### Загрузка в удаленный Nexus

`**gradlew clean uploadArchives**`



## Подключение к проекту

Модуль загружен в изолированный репозиторий:

http://1.0.0.137:8010/nexus/content/groups/cuba-group/

### Подключение с использование Studio:

*Project Properties -> Edit -> App components -> Custom components -> Add -> Reviews*

`com.company.reviews:reviews-global:<current_version>`



### Создание таблиц

После подключения модуля к проекту необходимо пересоздать базу. Если это невозможно по какой, то причине, то необходимо выполнить все скрипты из 10 и 20.

Данные скрипты объедененны и представленны ниже.

```sql
create table REVIEWS_REVIEW (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    REVIEW text not null,
    AUTHOR_ID uuid,
    PARENT uuid,
    GRADE integer not null,
    --
    primary key (ID)
)^

alter table REVIEWS_REVIEW add constraint FK_REVIEWS_REVIEW_AUTHOR foreign key (AUTHOR_ID) references SEC_USER(ID)^
create index IDX_REVIEWS_REVIEW_AUTHOR on REVIEWS_REVIEW (AUTHOR_ID)^
```

#### Создание с использованием Studio

Воспользуйтесь функцией **Generate DB scripts** в разделе **DATA MODEL**. В секции *updates* создайте новый скрипт обновления - **New update script**. Вставте содержание скриптов 10 и 20 в *Script source* и выполните **Update database**. 

## Использование

Для того, чтобы использовать модуль для конкретного объекта необходимо включить его как *frame* в экран объекта.

### Сервисы

Модуль имеет сервисы, автоматизирующие некторые типовые задачи.

#### calculateRating - получение средней оценки

Примеры использования:

```java
public class TestEdit extends AbstractEditor<Test> {

    @Inject
    private ReviewService reviewService;

    @Override
    protected boolean preCommit() {
        getItem().setGrade(reviewService.calculateRating(getItem()));
        return true;
    }

}

```

```java
public class TestBrowse extends AbstractLookup {

    @Inject
    private GroupDatasource<Test, UUID> testsDs;

    @Inject
    private ReviewService reviewService;
    
    @Override
    public void init(Map<String, Object> params) {
        super.init(params);
    
        testsDs.addItemChangeListener(e -> {
            showNotification(reviewService.calculateRating(e.getItem()).toString());
        });
    }

}
```

