package com.company.reviews.service;

import com.company.reviews.core.AverageRatingCount;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import javax.transaction.Transactional;

@Service(ReviewService.NAME)
public class ReviewServiceBean implements ReviewService {
    @Inject
    private AverageRatingCount averageRatingCount;

    @Override
    @Transactional
    public Double calculateRating(Object object) {
        return averageRatingCount.countAverage(object);
    }
}