package com.company.reviews.service;


public interface ReviewService {
    String NAME = "reviews_ReviewService";

    Double calculateRating(Object object);
}